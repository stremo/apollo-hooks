import * as React from 'react';
import {ReactNode} from "react";

export interface IPageDataProps {
    title?: string
    children: ReactNode | ReactNode[]
}


export const Page = ({title, children}: IPageDataProps) => {
    return <div>
        {title && <h1>{title}</h1>}

        {children}
    </div>
};
