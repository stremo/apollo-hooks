import * as React from 'react';

import { ApolloProvider } from 'react-apollo-hooks';
import {
    Router,
    Route,
    Switch
} from "react-router-dom";
import history from './history/history';

import {Row, Col} from "antd";

import "antd/dist/antd.css";  // or 'antd/dist/antd.less'
import './App.scss';

import {NotFoundPage} from "./components/NotFoundPage/NotFoundPage";
import {ArticleOverview} from "./components/ArticleOverview/ArticleOverview";
import {ArticleDetailPage} from "./components/ArticleDetailPage/ArticleDetailPage";

export interface IAppDataProps {
    client: any
}

export class App extends React.Component<IAppDataProps, any> {

    public render() {
        return (
                <div className="App-App">
                    <ApolloProvider client={this.props.client}>
                        <Router history={history}>
                            <div>
                                <div className="App-AppPage">
                                    <Row>
                                        <Col span={22} offset={1}>
                                            <Switch>
                                                <Route exact path="/" component={ArticleOverview}/>
                                                <Route exact path="/articles/:articleId" component={ArticleDetailPage}/>
                                                <Route path="/" component={NotFoundPage}/>
                                            </Switch>
                                        </Col>
                                    </Row>
                                </div>
                            </div>
                        </Router>
                    </ApolloProvider>
                </div>
        );
    }
}
