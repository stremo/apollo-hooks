import uuid from "uuid/v4";

import Article from "./../models/Article"
import ArticleTag from "./../models/ArticleTag";

ArticleTag.create({
  name: "GraphQL",
  color: "#dd2255"
});


ArticleTag.create({
  name: "JavaScript",
  color: "#dd5c25"
});

ArticleTag.create({
  name: "Idea",
  color: "#b7dd47"
});

ArticleTag.create({
  name: "REST",
  color: "#3d6ddd"
});


ArticleTag.create({
  name: "Java",
  color: "#dd3a90"
});

ArticleTag.create({
  name: "Microservices",
  color: "#ddd259"
});

Article.create({
  uuid: "385343bc-dab1-4f10-a4d6-adc1256ab9ed",
  title: "dummyArticle",
  date: "2018-07-05 07:55",
  tags_ids: [1, 2, 3],
  language: "de",
  abstract: "abstract",
  content: "md file content comes later",
  likes: 0
});

Article.create({
  uuid: "fd74a735-3b83-4eb9-94b6-a8ecaf7bb258",
  title: "dummyArticle 2",
  date: "2018-07-05 07:55",
  language: "de",
  abstract: "abstract",
  content: "md file content comes later",
  likes: 0
});

Article.create({
  uuid: "d102d2e6-f643-40c8-be0e-8241e01f0eef",
  title: "dummyArticle 3",
  tags_ids: [1, 2, 3, 4, 5, 6],
  date: "2018-07-05 07:55",
  language: "de",
  abstract: "abstract",
  content: "md file content comes later",
  likes: 0
});

Article.create({
  uuid: "e92b8a45-a0a3-44a2-93fd-b9a3857fcd4f",
  title: "dummyArticle 4",
  tags_ids: [1, 2, 3, 4, 5, 6],
  date: "2018-07-05 07:55",
  language: "de",
  abstract: "abstract",
  content: "md file content comes later",
  likes: 0
});

Article.create({
  uuid: "0175d359-d564-461c-8554-b98df75c23c9",
  title: "dummyArticle 5",
  tags_ids: [1, 2, 3, 4, 5, 6],
  date: "2018-07-05 07:55",
  language: "de",
  abstract: "abstract",
  content: "md file content comes later",
  likes: 0
});


const resolverFunctions = {
  Query: {
    articles() {
      return Article.findAll();
    },
    article(_: null, {id}: { id: string }) {
      return Article.findByUUID(id);
    },
  },
  Mutation: {
    likeArticle(_: null, {articleId}: { articleId: string }) {
      const article = Article.findByUUID(articleId);
      if (!article) {
        console.warn(`article with id uuid ${articleId} was not found`);
        return null;
      }
      Article.updateByID(articleId, {
        likes: article.likes++,
      });
      return Article.findByUUID(articleId);

    },
  },
  Article: {
    tags(root: any) {
      if (!root.tags_ids || root.tags_ids.length === 0)
        return;

      let res: Array<any> = [];
      root.tags_ids.forEach((item: any) => {
        res.push(ArticleTag.findByID(item))
      });
      return res;
    },
  },
  ArticleTag: {
    numberOfArticles(root: any) {
      let tagId = root.id;
      let articleCounter = 0;
      Article.findAll().forEach((article: any, index: number) => {
        if (article.tags_ids && article.tags_ids.includes(tagId)) {
          articleCounter++;
        }
      });
      return articleCounter;
    },
    articles(root: any): any[] {
      let tagId = root.id;
      let articles: any[] = [];
      Article.findAll().forEach((article: any) => {
        if (article.tags_ids && article.tags_ids.includes(tagId)) {
          articles.push(article);
        }
      });
      return articles;
    }
  }
};

export default resolverFunctions;