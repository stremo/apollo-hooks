import {ApolloServer} from "apollo-server-express";
import schema from "./graphql/schema";
import express from "express";
import * as bodyParser from "body-parser";

const app = express();
app.use(bodyParser.json());

const apolloServer = new ApolloServer({
  schema: schema,
  playground: true
});

apolloServer.applyMiddleware({app});

app.listen(8080, () => {
    console.log("server started at port http://localhost:8080");
});