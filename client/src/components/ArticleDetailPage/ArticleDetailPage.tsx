import * as React from "react";
import {useArticleQuery} from "../../graphql/queries";
import {LoadingDisplay} from "../LoadingDisplay/LoadingDisplay";
import {GetArticle_article} from "../../graphql/__generated__/GetArticle";
import {Col, Row, Tag} from "antd";
import {LikeArticleButton} from "../LikeArticleButton/LikeArticleButton";

export const ArticleDetailPage = (props: any) => {
  if (!props.match || !props.match.params || !props.match.params.articleId) {
    return <div>errors: no article id given</div>
  }

  const {loading, errors, data} = useArticleQuery(props.match.params.articleId);

  if (loading) {
    return <LoadingDisplay/>
  }

  if (errors) {
    return <div>errors</div>
  }

  if (!data || !data.article) {
    return <div> no article with given id found</div>
  }


  return renderArticle(data.article)

};

const renderArticle = (article: GetArticle_article) => {
  return (
      <div>
        <Row>
          <Col span={24}>
            <h1>{article.title}{' '}
              <small>vom {article.date}  ({article.likes} Personen gefällt das)<LikeArticleButton articleId={article.uuid}/></small>
            </h1>
          </Col>
        </Row>
        {
          article.tags && (
              <Row>
                <Col span={24}>
                  <div>
                    {
                      article.tags.map((tag, index) => <Tag key={`${index}`} color={tag.color}>{tag.name}</Tag>)
                    }
                  </div>
                </Col>
              </Row>
          )
        }

        <Row>
          <h5>Abstract: </h5>
          <Col span={24}>
            {article.abstract}
          </Col>

        </Row>

        <hr/>
        <Row>
          <Col span={24}>
            {article.content}
          </Col>

        </Row>

      </div>
  )
};
