
interface tableEntry {
    id: number
    uuid: string
}

interface paramsObject {
    where?: any
    whereNot?: any
    startsWith?: any
    limit?: number
}


let database: any = {};

let lastIndex: any = {};



/**
 * @todo Refactoring!
 */
class BaseEntity {

    static tableName: string = "baseEntities";


    constructor() {

    }

    static getTableName(): string {
        return this.tableName;
    }



    static table(): any[] {
        if (!database[this.getTableName()]) {
            database[this.getTableName()] = []
        }

        return database[this.getTableName()];
    }

    static index(): number {
        if (!lastIndex[this.getTableName()]) {
            lastIndex[this.getTableName()] = 0;
        }
        lastIndex[this.getTableName()]++;
        return lastIndex[this.getTableName()];
    };


    static getLastIndex(): number {
        return lastIndex[this.getTableName()];
    }

    static findByID(id: string | number) {
        return this.table().filter((item: any) => item.id === id)[0];

    }

    static findByUUID(id: string) {

        return this.table().filter((item: any) => item.uuid === id)[0];
    }

    static findAll(params: paramsObject = {}) {

        const {where, whereNot, startsWith, limit} = params;
        let result : any= this.table();

        if (where) {//todo: refactoring
            result = result.filter((item: any) => {
                let sR = true;
                Object.keys(where).forEach((key: string) => {
                    if (item[key] !== where[key]) {
                        sR = false;
                    }
                });
                return sR;
            })
        }

        if (whereNot) { //todo: refactoring
            result = result.filter((item: any) => {
                let sR: boolean = true;
                Object.keys(whereNot).forEach((key: string) => {
                    if (item[key] === whereNot[key]) {
                        sR = false;
                    }
                });
                return sR;
            })
        }

        if (startsWith) {
            result = result.filter((item: any) => {
                let sR = true;
                Object.keys(startsWith).forEach(key => {
                    if (!item[key].startsWith(startsWith[key])) {
                        sR = false;
                    }
                });
                return sR;
            })
        }

        if (limit) {
            return result.slice(0, limit)
        }

        return result;
    }


    static findOne(params: paramsObject = {}) {
        params.limit = 1;
        return this.findAll(params)[0];

    }


    static create(data: any  = {}) {
        data.id = this.index();
        this.table().push(data);
        return data;
    }

    static updateByID(id: string | number, data: object) {
        let index = this.table().findIndex((x: any) => x.id  === id);

        if(index === -1 )
            return null;

        let entry = this.table()[index];

        if(!entry)
            return null;

        entry = Object.assign(entry, data);
        this.table()[index] = entry;
        return entry;

    }

    static updateByUUID(uuid: string, data: object) {
        let index: number = this.table().findIndex((x: any) => x.uuid === uuid);
        let entry: object = this.table()[index];

        entry = Object.assign(entry, data);
        this.table()[index] = entry;
        return entry;

    }

    static drop(id: string | number) {
        if (!id)
            return;

        let index = this.table().findIndex((x: any) => x.id === id);
        if(index === -1) {
            return;
        }

        this.table().splice(index, 1);
    }


    static dropByUUID(uuid: string) {
        if (!uuid)
            return;

        let index = this.table().findIndex((x: any) => x.uuid === uuid);
        if(index === -1) {
            return;
        }

        this.table().splice(index, 1);
    }

    static count() {
        return this.table().length;
    }


}

export default BaseEntity;

export {
    database
}