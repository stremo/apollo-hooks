# Apollo Hooks Beispiel

Das Repo dient als Beispiel für die Verwendung von apollo-react-hooks. 

Dabei gibt es ein GraphQL-Backend (Ordner server) und ein React Client (Ordner client).

Als Beispiel werden Artikel angezeigt. Dabei kann eine Liste von Artikeln angezeigt werden sowie ein einzelner Artikel in der Detailansicht. 

Ein Beispiel für Mutations ist, dass der Leser ein Artikel liken kann (Thumps Up Icon rechts oben in der Detailansicht). 