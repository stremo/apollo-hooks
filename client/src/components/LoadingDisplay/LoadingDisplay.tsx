import * as React from "react";
import {Spin} from "antd";

export const LoadingDisplay = () => {
  return <div>
    <Spin/> loading...
  </div>
};
