import * as React from "react";
import {Icon} from 'antd';
import {likeArticleMutation} from "../../graphql/mutations";

export interface LikeArticleButtonProps {
  articleId: string
}

export const LikeArticleButton = ({articleId}: LikeArticleButtonProps) => {

  const mutation = likeArticleMutation(articleId);

  const onClick= () => {
    mutation()
  };

  return (
      <Icon type="like" style={{cursor: "pointer"}} onClick={onClick}/>
  )

};
