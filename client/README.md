# Clientseitige Anwendung

Es gibt zwei Ansichten. Eine stellt die verschiedenen Artikel da. Die Zweite ist die Detailansicht für den jeweiligen Artikel

| Hinweiß für die Installation. Bevor man anfangen kann zu entwickeln, muss "Generierung der Typen für Typescript aus dem Backend" durchgeführt werden

## apollo-react-hooks

Es werden bei allen Abfragen react-hooks verwendet.
Dabei fidnen sich die GraphQL Abfragen im Ordner `src/graphql`

## Generierung der Typen für Typescript aus dem Backend

Um die Typescript Definitions für das GraphQL Schema zu erzeugen, sind folgende Schritte notwendig

1. Install Apollo Cli `npm install -g apollo`
2. start backend server
3. run `npm run generateGraphQLTypes`

Nachdem man das Schema auf dem Server oder an den clientseitigen Queries vorgenommen hat, muss man immer `npm run generateGraphQLTypes` für die Genierierung der Typescript definitions ausführen.


## Links

Weiteres Beispiel für apollo-react-hooks: 
[Thomas Farla: using react hooks in apollo](https://medium.com/@thomasfarla/using-react-hooks-in-apollo-47ff2a9fde2f)
