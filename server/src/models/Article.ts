import BaseEntity from './BaseEntity';

export default class Article extends BaseEntity {
    static tableName: string = 'articles';
}