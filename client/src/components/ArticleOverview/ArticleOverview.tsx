import * as React from "react";
import {Link} from "react-router-dom";
import {ArticleList} from "./ArticleList/ArticleList";
import {Col, Row} from "antd";

import "./ArticleOverview.scss";
import {Page} from "../Page/Page";

export const ArticleOverview = () => {
  return (
      <Page>
        <div className="article-overview">
          <h1>Artikel</h1>
          <Row>
            <Col span={24}>
              <ArticleList/>
            </Col>
          </Row>
        </div>
      </Page>
  )
};
