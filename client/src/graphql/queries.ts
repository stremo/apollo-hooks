import gql from "graphql-tag";
import {useQuery} from "react-apollo-hooks";
import {GetArticles} from "./__generated__/GetArticles";
import {GetArticle, GetArticleVariables} from "./__generated__/GetArticle";

export const getArticlesQuery = gql`
    query GetArticles {
        articles {
            uuid
            title
            date
            abstract
            tags {
                name
                color
            }
        }
    }
`;

export const useArticlesQuery = () => useQuery<GetArticles, {}>(getArticlesQuery);

export const getArticleQuery = gql`
    query GetArticle($id: ID!) {
        article(id: $id) {
            uuid
            title
            date
            abstract
            content
            tags {
                name
                color
            }
            likes
        }
    }
`;

export const useArticleQuery = (articleId: string) => useQuery<GetArticle, GetArticleVariables>(getArticleQuery, {
  variables: {id: articleId}
});
