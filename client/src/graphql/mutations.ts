import gql from 'graphql-tag';
import {useMutation} from 'react-apollo-hooks';
import {LikeArticle, LikeArticleVariables} from "./__generated__/LikeArticle";

export const likeArticle = gql`
    mutation LikeArticle($articleId: ID!) {
        likeArticle(articleId: $articleId) {
            likes
        }
    }
`;

export const likeArticleMutation = (id: string) => useMutation<LikeArticle, LikeArticleVariables>(likeArticle, {
  refetchQueries: ["GetArticle"],
  variables: {articleId: id},
});
