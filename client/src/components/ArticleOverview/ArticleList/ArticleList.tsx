import * as React from "react";
import {ColumnProps} from "antd/lib/table";
import {Table, Tag} from "antd";
import history from "./../../../history/history";
import {LoadingDisplay} from "../../LoadingDisplay/LoadingDisplay";
import {useArticlesQuery} from "../../../graphql/queries";
import {GetArticles_articles} from "../../../graphql/__generated__/GetArticles";

export const ArticleList = () => {

  const {data, loading, errors} = useArticlesQuery();

  if (loading) {
    return <LoadingDisplay/>
  }

  if (errors) {
    return <div>errors: {errors}</div>
  }

  const articles = data && data.articles || [];

  const columns: ColumnProps<GetArticles_articles>[] = [
    {
      dataIndex: 'title',
      key: 'title',
      title: "Titel",
    },
    {
      dataIndex: 'date',
      key: 'date',
      title: "Datum",
    },
    {
      key: "tags",
      render: (text, article) => {
        if (!article.tags) {
          return;
        }
        return (
            <div>
              {
                article.tags.map((tag, index) => <Tag key={`${index}`} color={tag.color}>{tag.name}</Tag>)
              }
            </div>
        )
      },
      title: "Tags",
    },
    {
      dataIndex: 'abstract',
      key: 'abstract',
      title: "Abstract",
    },

  ];

  return (
      <div>
        <Table
            dataSource={articles}
            columns={columns}
            rowKey={(item, index) => `${index}`}
            onRow={(article) => {
              return {
                onClick: () => {
                  history.push(`/articles/${article.uuid}`);
                },
              }
            }
            }
            pagination={false}/>
      </div>
  )
};
