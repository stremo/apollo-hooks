import * as React from 'react';

import {Page} from "../Page/Page";

export const NotFoundPage = () => {
    return (
        <Page>
            <h1>404 not found</h1>

            content not found
        </Page>
    );
};
