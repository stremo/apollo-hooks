import * as React from 'react';
import * as ReactDOM from 'react-dom';

import {InMemoryCache} from 'apollo-cache-inmemory';

import "antd/dist/antd.css";

import './index.scss';
import {App} from "./App";
import registerServiceWorker from './registerServiceWorker';
import ApolloClient from 'apollo-client';
import {HttpLink} from 'apollo-link-http';

// This is the same cache you pass into new ApolloClient
const cache = new InMemoryCache();
const uri = `http://localhost:3000/graphql`;

const link = new HttpLink({uri});
const client = new ApolloClient({
  cache,
  link
});

ReactDOM.render(
    <App client={client}/>,
    document.getElementById('root') as HTMLElement
);

registerServiceWorker();
