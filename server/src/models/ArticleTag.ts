import BaseEntity from './BaseEntity';

export default class ArticleTag extends BaseEntity {
    static tableName: string = 'articleTag';
}